# Quickstart for Drupal and Next.js

This repository contains a quickstart for Drupal 9.4 and a Next.js headless application.

## Installation

1. Run `make` or `docker-compose up -d` in order to start containers.

2. Download composer dependencies by running `make composer install` or `docker-compose exec php composer install`.

3. Then go to http://api.next.docker.localhost and install Drupal from the existing configuration.

4. Add some content.

## Usage

Drupal API is available at http://api.next.docker.localhost. You can add content and see the preview on the node page.

Next.js page is available at http://next.docker.localhost. It will show all content generated from Drupal.


## Used stuff

Drupal - https://www.drupal.org

Next.js for Drupal - https://next-drupal.org

Docker4Drupal - https://github.com/wodby/docker4drupal
